package com.zx.mes.feign.hyl;

import com.zx.mes.feign.hyl.pagemodel.LoginAppUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 用户 feign
 *
 * @author 华云龙
 * @date 2018-12-17
 */
@FeignClient("long-oauth2-upms")
public interface UserClient {

    /**
     * 获取一个用户详细信息
     *
     * @param username String
     * @return LoginAppUser
     */
    @GetMapping(value = "/users-anon/internal", params = "username")
    LoginAppUser findByUsername(@RequestParam("username") String username);

}
