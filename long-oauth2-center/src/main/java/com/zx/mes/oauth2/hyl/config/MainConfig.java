package com.zx.mes.oauth2.hyl.config;

import org.springframework.context.annotation.Configuration;

/**
 * 主配置文件
 *
 * @author 华云龙
 * @date 2018-12-17
 */
@Configuration
public class MainConfig {
}
