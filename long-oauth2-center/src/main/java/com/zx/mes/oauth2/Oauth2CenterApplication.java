package com.zx.mes.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * oauth2 center auth
 *
 * @author 华云龙
 * @date 2018-12-17
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.zx.mes"})
public class Oauth2CenterApplication {
    public static void main(String[] args) {
        SpringApplication.run(Oauth2CenterApplication.class, args);
    }
}
