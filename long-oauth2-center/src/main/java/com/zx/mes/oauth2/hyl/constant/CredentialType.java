package com.zx.mes.oauth2.hyl.constant;

/**
 * 用户账号类型
 *
 * @author 华云龙
 * @date 2018-12-17
 */
public enum CredentialType {

    /**
     * 用户名
     */
    USERNAME,
    /**
     * 手机号
     */
    PHONE,
    /**
     * 微信openid
     */
    WECHAT_OPENID,
}
