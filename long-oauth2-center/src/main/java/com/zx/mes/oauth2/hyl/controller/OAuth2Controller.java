package com.zx.mes.oauth2.hyl.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 华云龙
 * @date 2018-12-17
 */
@Slf4j
@RestController
@RequestMapping
public class OAuth2Controller {

    /**
     * 当前登陆用户信息<br>
     * <p>
     * security获取当前登录用户的方法是SecurityContextHolder.getContext().getAuthentication()<br>
     * 返回值是接口org.springframework.security.core.Authentication，又继承了Principal<br>
     * 这里的实现类是org.springframework.security.oauth2.provider.OAuth2Authentication<br>
     * <p>
     *
     * @return
     */
    @GetMapping("/user-me")
    public Authentication principal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        log.debug("user-me:{}", authentication.getName());
        return authentication;
    }


//
//    /**
//     * 注销登陆/退出
//     * 移除access_token和refresh_token<br>
//     * 2018.06.28 改为用ConsumerTokenServices，该接口的实现类DefaultTokenServices已有相关实现，我们不再重复造轮子
//     *
//     * @param access_token
//     */
//    @DeleteMapping(value = "/remove_token", params = "access_token")
//    public void removeToken(String access_token) {
//        boolean flag = tokenServices.revokeToken(access_token);
//        if (flag) {
//            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//
//        }
//    }


}
