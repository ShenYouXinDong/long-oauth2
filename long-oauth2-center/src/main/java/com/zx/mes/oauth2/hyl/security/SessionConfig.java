package com.zx.mes.oauth2.hyl.security;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 开启session共享
 *
 * @author 华云龙
 * @date 2018-12-17
 */
@EnableRedisHttpSession
public class SessionConfig {
}
