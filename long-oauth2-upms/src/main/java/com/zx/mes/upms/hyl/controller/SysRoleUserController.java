package com.zx.mes.upms.hyl.controller;


import com.zx.mes.hyl.controller.BaseMethod;
import com.zx.mes.hyl.response.ObjectRestResponse;
import com.zx.mes.hyl.response.TableResultResponse;
import com.zx.mes.upms.hyl.entity.SysRoleUser;
import com.zx.mes.upms.hyl.service.SysRoleUserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色用户关系表 前端控制器
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@RestController
@RequestMapping("/sysRoleUser")
public class SysRoleUserController extends BaseMethod<SysRoleUserService, SysRoleUser> {

    /**
     * 使用要求:所有的表都必需有唯一key值,且名称为 REFLECTION_FIELD_NAME (id)
     *
     * @param entity 实体类(pojo)
     * @return ObjectRestResponse
     */
    @PreAuthorize("hasAuthority('sysRoleUser:save')")
    @ApiOperation(value = "添加一笔数据", notes = "根据id自生成(数据库),还是前端生成(自身成36位)")
    @PostMapping(value = "/add")
    public ObjectRestResponse<SysRoleUser> save(@RequestBody SysRoleUser entity) {
        return super.add(entity);
    }

    @PreAuthorize("hasAuthority('sysRoleUser:query')")
    @ApiOperation(value = "根据所传id(数据库表的主键必需是id才能用)")
    @GetMapping(value = "getOne/{id}")
    public ObjectRestResponse<SysRoleUser> getById(@ApiParam("id 是数据库的唯一主键") @PathVariable String id) {
        return super.get(id);
    }

    /**
     * {
     * "status": 200,
     * "message": "数据更新成功!",
     * "data": null,
     * "rel": true
     * }
     *
     * @param entity 对应实体类
     * @return ObjectRestResponse
     */
    @PreAuthorize("hasAuthority('sysRoleUser:update')")
    @ApiOperation(value = "单笔更新操作", notes = "必需根据id来更新")
    @PutMapping(value = "update")
    public ObjectRestResponse<SysRoleUser> updateByEntity(@RequestBody SysRoleUser entity) {
        return super.update(entity);
    }

    @PreAuthorize("hasAuthority('sysRoleUser:del')")
    @ApiOperation(value = "单笔删除操作", notes = "必需根据id来删除")
    @DeleteMapping(value = "delete/{id}")
    public ObjectRestResponse<SysRoleUser> del(@PathVariable String id) {
        return super.remove(id);
    }

    @PreAuthorize("hasAuthority('sysRoleUser:query')")
    @ApiOperation(value = "获取所有的数据", notes = "一般不推荐使用,小数据量可以")
    @GetMapping(value = "/all")
    public List<SysRoleUser> getAll() {
        return super.all();
    }

    @PreAuthorize("hasAuthority('sysRoleUser:query')")
    @ApiOperation(value = "分页操作", notes = "params HashMap example = \"page=1&limit=5\"")
    @SuppressWarnings({"unchecked"})
    @GetMapping(value = "/page")
    public TableResultResponse page(@RequestParam Map<String, Object> params, SysRoleUser entity) {
        return super.list(params, entity);
    }

}

