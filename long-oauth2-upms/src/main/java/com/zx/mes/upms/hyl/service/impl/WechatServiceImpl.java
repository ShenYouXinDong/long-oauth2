package com.zx.mes.upms.hyl.service.impl;

import com.zx.mes.upms.hyl.entity.Wechat;
import com.zx.mes.upms.hyl.mapper.WechatMapper;
import com.zx.mes.upms.hyl.service.WechatService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 微信信息表 服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Service
public class WechatServiceImpl extends ServiceImpl<WechatMapper, Wechat> implements WechatService {

}
