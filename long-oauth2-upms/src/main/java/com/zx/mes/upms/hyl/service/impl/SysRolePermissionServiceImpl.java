package com.zx.mes.upms.hyl.service.impl;

import com.zx.mes.upms.hyl.entity.SysRolePermission;
import com.zx.mes.upms.hyl.mapper.SysRolePermissionMapper;
import com.zx.mes.upms.hyl.service.SysRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限关系表 服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements SysRolePermissionService {

}
