package com.zx.mes.upms.hyl.service;

import com.zx.mes.upms.hyl.entity.SysRoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色用户关系表 服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface SysRoleUserService extends IService<SysRoleUser> {

}
