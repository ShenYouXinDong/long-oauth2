package com.zx.mes.upms.hyl.mapper;

import com.zx.mes.upms.hyl.entity.AppUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface AppUserMapper extends BaseMapper<AppUser> {

}
