package com.zx.mes.upms.hyl.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.mes.hyl.controller.BaseMethod;
import com.zx.mes.hyl.pagemodel.PageParam;
import com.zx.mes.hyl.response.ObjectRestResponse;
import com.zx.mes.hyl.response.TableResultResponse;
import com.zx.mes.upms.hyl.entity.SysPermission;
import com.zx.mes.upms.hyl.entity.SysServicePermission;
import com.zx.mes.upms.hyl.service.SysPermissionService;
import com.zx.mes.upms.hyl.service.SysServicePermissionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 权限标识表 前端控制器
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@RestController
@RequestMapping("/sysPermission")
public class SysPermissionController extends BaseMethod<SysPermissionService, SysPermission> {


    @Autowired
    private SysServicePermissionService sysServicePermissionService;
    /**
     * 使用要求:所有的表都必需有唯一key值,且名称为 REFLECTION_FIELD_NAME (id)
     *
     * @param entity 实体类(pojo)
     * @return ObjectRestResponse
     */
    @PreAuthorize("hasAuthority('back:permission:save')")
    @ApiOperation(value = "添加一笔数据", notes = "根据id自生成(数据库),还是前端生成(自身成36位)")
    @PostMapping(value = "/add")
    public ObjectRestResponse<SysPermission> save(@RequestBody SysPermission entity) {
        entity.setCreateTime(LocalDateTime.now()).setUpdateTime(LocalDateTime.now());
        return super.add(entity);
    }

    @PreAuthorize("hasAuthority('back:permission:query')")
    @ApiOperation(value = "根据所传id(数据库表的主键必需是id才能用)")
    @GetMapping(value = "getOne/{id}")
    public ObjectRestResponse<SysPermission> getById(@ApiParam("id 是数据库的唯一主键") @PathVariable String id) {
        return super.get(id);
    }

    /**
     * @param entity 对应实体类
     * @return ObjectRestResponse
     */
    @PreAuthorize("hasAuthority('back:permission:update')")
    @ApiOperation(value = "单笔更新操作", notes = "必需根据id来更新")
    @PutMapping(value = "update")
    public ObjectRestResponse<SysPermission> updateByEntity(@RequestBody SysPermission entity) {
        entity.setUpdateTime(LocalDateTime.now());
        return super.update(entity);
    }

    @PreAuthorize("hasAuthority('back:permission:delete')")
    @ApiOperation(value = "单笔删除操作", notes = "必需根据id来删除")
    @DeleteMapping(value = "delete/{id}")
    public ObjectRestResponse<SysPermission> del(@PathVariable String id) {
        this.sysServicePermissionService.remove(new QueryWrapper<SysServicePermission>().lambda().eq(SysServicePermission::getPermissionId, id));
        return super.remove(id);
    }

    @PreAuthorize("hasAuthority('back:permission:query')")
    @ApiOperation(value = "获取所有的数据", notes = "一般不推荐使用,小数据量可以")
    @GetMapping(value = "/all")
    public List<SysPermission> getAll() {
        return super.all();
    }

    @PreAuthorize("hasAuthority('back:permission:query')")
    @ApiOperation(value = "分页操作", notes = "params HashMap example = \"page=1&limit=5\"")
    @SuppressWarnings({"unchecked"})
    @GetMapping(value = "/page")
    public TableResultResponse page(@RequestParam Map<String, Object> params, SysPermission entity) {
        TableResultResponse.Builder<SysPermission> builder = new TableResultResponse.Builder();
        PageParam extPageParams = this.baseControllerExt.getPageParams(params);
        QueryWrapper<SysPermission> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(entity.getPermission())) {
            wrapper.lambda().like(SysPermission::getPermission, entity.getPermission());
        }
        List<SysPermission> list = this.biz.page(new Page((long)extPageParams.getPage(), (long)extPageParams.getLimit()), wrapper).getRecords();
        if (null != list && list.size() != 0) {
            builder.message("分页查询结果").total((long)list.size()).total((long)this.biz.count(wrapper)).page((long)extPageParams.getPage()).rows(list);
        } else {
            builder.message("无数据").page(0L).rows(null);
        }

        return builder.build();
    }

    @PreAuthorize("hasAuthority('back:permission:delete')")
    @DeleteMapping("/deleteBatch")
    public ObjectRestResponse<SysPermission> deleteBatch(String ids) {
        ObjectRestResponse.Builder<SysPermission> response = new ObjectRestResponse.Builder<>();
        List<String> list = this.baseControllerExt.divideStrToList(ids);
        this.biz.removeByIds(list);
        response.message("批量删除成功!");
        return response.build();
    }

    @PreAuthorize("hasAuthority('test:permission:test')")
    @GetMapping("/test")
    public ObjectRestResponse<SysPermission> test(){
        ObjectRestResponse.Builder<SysPermission> response = new ObjectRestResponse.Builder<>();
        response.message("这是任何人也没有权限访问的,做为权限测试之用!");
        return response.build();
    }
}

