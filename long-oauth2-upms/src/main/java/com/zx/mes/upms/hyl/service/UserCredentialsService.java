package com.zx.mes.upms.hyl.service;

import com.zx.mes.upms.hyl.entity.UserCredentials;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户凭证表 服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface UserCredentialsService extends IService<UserCredentials> {

}
