package com.zx.mes.upms.hyl.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zx.mes.upms.hyl.entity.AppUser;
import com.zx.mes.upms.hyl.mapper.AppUserMapper;
import com.zx.mes.upms.hyl.service.AppUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Service
public class AppUserServiceImpl extends ServiceImpl<AppUserMapper, AppUser> implements AppUserService {

}
