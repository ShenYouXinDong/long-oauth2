package com.zx.mes.upms.hyl.mapper;

import com.zx.mes.upms.hyl.entity.SysRoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色用户关系表 Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUser> {

}
