package com.zx.mes.upms.hyl.mapper;

import com.zx.mes.upms.hyl.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限标识表 Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

}
