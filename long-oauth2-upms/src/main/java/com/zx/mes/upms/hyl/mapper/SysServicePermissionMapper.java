package com.zx.mes.upms.hyl.mapper;

import com.zx.mes.upms.hyl.entity.SysServicePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-25
 */
public interface SysServicePermissionMapper extends BaseMapper<SysServicePermission> {

}
