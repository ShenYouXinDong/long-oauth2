package com.zx.mes.upms.hyl.service;

import com.zx.mes.upms.hyl.entity.Wechat;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 微信信息表 服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface WechatService extends IService<Wechat> {

}
