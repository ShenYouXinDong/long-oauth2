package com.zx.mes.upms.hyl.service.impl;

import com.zx.mes.upms.hyl.entity.SysServicePermission;
import com.zx.mes.upms.hyl.mapper.SysServicePermissionMapper;
import com.zx.mes.upms.hyl.service.SysServicePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-25
 */
@Service
public class SysServicePermissionServiceImpl extends ServiceImpl<SysServicePermissionMapper, SysServicePermission> implements SysServicePermissionService {

}
