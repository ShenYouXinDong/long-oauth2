package com.zx.mes.upms.hyl.service;

import com.zx.mes.upms.hyl.entity.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限关系表 服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface SysRolePermissionService extends IService<SysRolePermission> {

}
