package com.zx.mes.upms.hyl.pagemodel;

import com.zx.mes.upms.hyl.entity.SysPermission;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * role page 包括了role与权限的信息
 *
 * @author 华云龙
 * @date 2018-12-24
 */
@Data
@Accessors(chain = true)
public class RolePage implements Serializable {

    private Integer id;

    private String code;

    private String name;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private List<SysPermission> list = new ArrayList<>(32);

}
