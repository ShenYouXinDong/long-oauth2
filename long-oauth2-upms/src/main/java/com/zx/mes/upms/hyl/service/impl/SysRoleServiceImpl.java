package com.zx.mes.upms.hyl.service.impl;

import com.zx.mes.upms.hyl.entity.SysRole;
import com.zx.mes.upms.hyl.mapper.SysRoleMapper;
import com.zx.mes.upms.hyl.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
