package com.zx.mes.upms.hyl.mapper;

import com.zx.mes.upms.hyl.entity.Wechat;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 微信信息表 Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface WechatMapper extends BaseMapper<Wechat> {

}
