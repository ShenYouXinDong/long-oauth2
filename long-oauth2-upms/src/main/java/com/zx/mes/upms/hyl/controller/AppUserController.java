package com.zx.mes.upms.hyl.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zx.mes.hyl.controller.BaseMethod;
import com.zx.mes.hyl.pagemodel.PageParam;
import com.zx.mes.hyl.response.ObjectRestResponse;
import com.zx.mes.hyl.response.TableResultResponse;
import com.zx.mes.upms.hyl.entity.*;
import com.zx.mes.upms.hyl.pagemodel.LoginAppUser;
import com.zx.mes.upms.hyl.service.*;
import com.zx.mes.upms.hyl.util.AppUserUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@RestController
public class AppUserController extends BaseMethod<AppUserService, AppUser> {


    @Autowired
    private SysRoleUserService sysRoleUserService;

    @Autowired
    private SysRoleService sysRoleService;

    @Autowired
    private SysRolePermissionService sysRolePermissionService;

    @Autowired
    private SysPermissionService sysPermissionService;

    @Autowired
    private UserCredentialsService userCredentialsService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * 使用要求:所有的表都必需有唯一key值,且名称为 REFLECTION_FIELD_NAME (id)
     *
     * @param entity 实体类(pojo)
     * @return ObjectRestResponse
     */
    @ApiOperation(value = "添加一笔数据", notes = "根据id自生成(数据库),还是前端生成(自身成36位)")
    @PostMapping(value = "/appUser/add")
    public ObjectRestResponse<AppUser> save(@RequestBody AppUser entity) {
        entity.setCreateTime(LocalDateTime.now())
                .setUpdateTime(LocalDateTime.now())
                .setPassword(bCryptPasswordEncoder.encode(entity.getPassword()));
        return super.add(entity);
    }

    @PreAuthorize("hasAuthority('back:user:query')")
    @ApiOperation(value = "根据所传id(数据库表的主键必需是id才能用)")
    @GetMapping(value = "/appUser/getOne/{id}")
    public ObjectRestResponse<AppUser> getById(@ApiParam("id 是数据库的唯一主键") @PathVariable String id) {
        return super.get(id);
    }

    /**
     * @param entity 对应实体类
     * @return ObjectRestResponse
     */
    @PreAuthorize("hasAuthority('back:user:update')")
    @ApiOperation(value = "单笔更新操作", notes = "必需根据id来更新")
    @PutMapping(value = "/appUser/update")
    public ObjectRestResponse<AppUser> updateByEntity(@RequestBody AppUser entity) {
        entity.setUpdateTime(LocalDateTime.now());
        return super.update(entity);
    }

    @PreAuthorize("hasAuthority('back:user:del')")
    @ApiOperation(value = "单笔删除操作", notes = "必需根据id来删除")
    @DeleteMapping(value = "/appUser/delete/{id}")
    public ObjectRestResponse<AppUser> del(@PathVariable String id) {
        return super.remove(id);
    }

    @PreAuthorize("hasAuthority('back:user:query')")
    @ApiOperation(value = "获取所有的数据", notes = "一般不推荐使用,小数据量可以")
    @GetMapping(value = "/appUser/all")
    public List<AppUser> getAll() {
        return super.all();
    }

    @PreAuthorize("hasAuthority('back:user:query')")
    @ApiOperation(value = "分页操作", notes = "params HashMap example = \"page=1&limit=5\"")
    @SuppressWarnings({"unchecked"})
    @GetMapping(value = "/appUser/page")
    public TableResultResponse page(@RequestParam Map<String, Object> params, AppUser entity) {
        TableResultResponse.Builder<AppUser> builder = new TableResultResponse.Builder();
        PageParam extPageParams = this.baseControllerExt.getPageParams(params);
        LambdaQueryWrapper<AppUser> lambda = new QueryWrapper<AppUser>().lambda();
        /// username like
        if (entity != null && StringUtils.isNotBlank(entity.getUsername())) {
            lambda.like(AppUser::getUsername, entity.getUsername());
        }
        /// nickName like
        if (entity != null && StringUtils.isNotBlank(entity.getNickname())) {
            lambda.like(AppUser::getNickname, entity.getNickname());
        }

        List<AppUser> list = this.biz.page(new Page((long) extPageParams.getPage(), (long) extPageParams.getLimit()), lambda).getRecords();
        if (null != list && list.size() != 0) {
            builder.message("分页查询结果").total((long) list.size()).total((long) this.biz.count(null)).page((long) extPageParams.getPage()).rows(list);
        } else {
            builder.message("无数据").page(0L).rows(null);
        }

        return builder.build();
    }

    @PreAuthorize("hasAuthority('back:user:del')")
    @DeleteMapping(value = "/appUser/deleteBatch")
    public ObjectRestResponse<AppUser> deleteBatch(@RequestParam String ids) {
        ObjectRestResponse.Builder<AppUser> response = new ObjectRestResponse.Builder<>();
        /// 删除用户时,中间表对应的关系也应删除
        /// 参数检查
        if (StringUtils.isNotBlank(ids)) {
            List<String> list = this.baseControllerExt.divideStrToList(ids);
            /// 1.删除用户-角色中间表关系
            this.sysRoleUserService.remove(new QueryWrapper<SysRoleUser>().lambda().in(SysRoleUser::getUserId, list));
            /// 2.删除 用户凭证中对应用户的数据
            this.userCredentialsService.remove(new QueryWrapper<UserCredentials>().lambda().in(UserCredentials::getUserId, list));
            /// 3.删除用户本身数据
            this.biz.removeByIds(list);
            response.message("批量删除成功!");
        } else {
            response.message("所传参数无效,请检查!");
        }
        return response.build();
    }

    @GetMapping(value = "/appUser/users-anon/internal", params = "username")
    public LoginAppUser findByUsername(String username) {
        QueryWrapper<AppUser> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(AppUser::getUsername, username);
        AppUser appUser = this.biz.getOne(wrapper);
        LoginAppUser loginAppUser = new LoginAppUser();

        if (appUser != null) {
            /// 用户信息赋值
            BeanUtils.copyProperties(appUser, loginAppUser);
            List<SysRoleUser> sysRoleUsers = this.sysRoleUserService.list(new QueryWrapper<SysRoleUser>()
                    .lambda().eq(SysRoleUser::getUserId, appUser.getId()));
            List<Integer> roleIdList = new ArrayList<>();
            for (int i = 0; i < sysRoleUsers.size(); i++) {
                roleIdList.add(sysRoleUsers.get(i).getRoleId());
            }

            /// 这个人是否有角色
            if (roleIdList.size() > 0) {
                List<SysRole> sysRoles = this.sysRoleService.list(new QueryWrapper<SysRole>().lambda().in(SysRole::getId, roleIdList));
                /// 角色赋值
                loginAppUser.setSysRoles(new HashSet<>(sysRoles));

                List<SysRolePermission> sysRolePermissions = this.sysRolePermissionService.list(new QueryWrapper<SysRolePermission>().lambda().in(SysRolePermission::getRoleId, roleIdList));

                /// 这个角色是否有权限
                if (sysRolePermissions.size() > 0) {
                    List<Integer> permissionIdList = new ArrayList<>();
                    for (int i = 0; i < sysRolePermissions.size(); i++) {
                        permissionIdList.add(sysRolePermissions.get(i).getPermissionId());
                    }
                    List<SysPermission> sysPermissionList = this.sysPermissionService.list(new QueryWrapper<SysPermission>().lambda().in(SysPermission::getId, permissionIdList));
                    Set<String> permissions = new HashSet<>();
                    for (int i = 0; i < sysPermissionList.size(); i++) {
                        permissions.add(sysPermissionList.get(i).getPermission());
                    }
                    /// 权限赋值
                    loginAppUser.setPermissions(permissions);
                }
            }
        }

        return loginAppUser;
    }

    /**
     * 当前登录用户 LoginAppUser
     */
    @GetMapping("/appUser/current")
    public ObjectRestResponse<LoginAppUser> getLoginAppUser() {
        ObjectRestResponse.Builder<LoginAppUser> response = new ObjectRestResponse.Builder<>();
        response.data(AppUserUtil.getLoginAppUser()).message("获取详细的用户信息成功!");
        return response.build();
    }

    /**
     * 用户授权角色
     *
     * @param appUser 用户
     * @return response
     */
    @PostMapping("/appUser/grantRole")
    public ObjectRestResponse<AppUser> grantRole(@RequestBody AppUser appUser) {
        ObjectRestResponse.Builder<AppUser> response = new ObjectRestResponse.Builder<>();
        /// 最直接的就是 1.先删除原来所有的角色,2.再将新的角色授权给用户

        /// 1.删除中间表中所有与userId相关的数据
        this.sysRoleUserService.remove(new QueryWrapper<SysRoleUser>().lambda().eq(SysRoleUser::getUserId, appUser.getUserId()));
        /// 2.解析要授权的角色,再插入中间表
        List<String> list = this.baseControllerExt.divideStrToList(appUser.getRoleIds());
        List<SysRoleUser> roleUserList = new ArrayList<>(8);
        list.forEach(item -> {
            SysRoleUser sysRoleUser = new SysRoleUser();
            sysRoleUser.setUserId(Integer.valueOf(appUser.getUserId())).setRoleId(Integer.valueOf(item));
            roleUserList.add(sysRoleUser);
        });
        this.sysRoleUserService.saveBatch(new ArrayList<>(roleUserList));
        response.message("授权成功!");
        return response.build();
    }

    @GetMapping("/appUser/getRoleByUserId")
    public ObjectRestResponse<AppUser> getRoleByUserId(String userId) {
        ObjectRestResponse.Builder<AppUser> response = new ObjectRestResponse.Builder<>();
        /// 1.从 角色-用户表中找到对应角色信息
        List<SysRoleUser> list = this.sysRoleUserService.list(new QueryWrapper<SysRoleUser>().lambda().eq(SysRoleUser::getUserId, userId));
        /// 2.从 角色表中获取详细信息
        if (list != null && list.size() > 0) {
            List<Integer> roleIdList = new ArrayList<>(4);
            list.forEach(item -> roleIdList.add(item.getRoleId()));
            List<SysRole> sysRoleList = this.sysRoleService.list(new QueryWrapper<SysRole>().lambda().in(SysRole::getId, new ArrayList<>(roleIdList)));
            response.message("获取角色成功!").data(sysRoleList);
        } else {
            response.message("未获取到用户角色的相关信息!");
        }
        return response.build();
    }

}

