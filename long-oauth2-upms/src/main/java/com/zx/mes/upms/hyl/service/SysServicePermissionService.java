package com.zx.mes.upms.hyl.service;

import com.zx.mes.upms.hyl.entity.SysServicePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-25
 */
public interface SysServicePermissionService extends IService<SysServicePermission> {

}
