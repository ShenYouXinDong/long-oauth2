package com.zx.mes.upms.hyl.service.impl;

import com.zx.mes.upms.hyl.entity.SysService;
import com.zx.mes.upms.hyl.mapper.SysServiceMapper;
import com.zx.mes.upms.hyl.service.SysServiceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-25
 */
@Service
public class SysServiceServiceImpl extends ServiceImpl<SysServiceMapper, SysService> implements SysServiceService {

}
