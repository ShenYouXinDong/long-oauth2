package com.zx.mes.upms.hyl.mapper;

import com.zx.mes.upms.hyl.entity.UserCredentials;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户凭证表 Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface UserCredentialsMapper extends BaseMapper<UserCredentials> {

}
