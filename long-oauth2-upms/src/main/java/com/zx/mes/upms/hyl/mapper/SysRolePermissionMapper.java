package com.zx.mes.upms.hyl.mapper;

import com.zx.mes.upms.hyl.entity.SysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关系表 Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
