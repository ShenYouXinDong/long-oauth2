package com.zx.mes.upms.hyl.pagemodel;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 树结构(无限层级的树)
 * @author huayunlong
 * @date 2018-7-11
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class TreeNode implements Serializable {
    /**
     * element id
     */
    private String id;
    private String key;
    /**
     * element label
     */
    private String label;
    private String value;
    private String icon;
    private String title;
    private String pid;
    private Integer type;
    private String url;
    private Integer seq;
    /**
     * element children
     */
    private List<TreeNode> children = new ArrayList<>();
}
