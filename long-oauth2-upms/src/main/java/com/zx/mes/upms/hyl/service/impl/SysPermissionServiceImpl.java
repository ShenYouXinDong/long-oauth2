package com.zx.mes.upms.hyl.service.impl;

import com.zx.mes.upms.hyl.entity.SysPermission;
import com.zx.mes.upms.hyl.mapper.SysPermissionMapper;
import com.zx.mes.upms.hyl.service.SysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限标识表 服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements SysPermissionService {

}
