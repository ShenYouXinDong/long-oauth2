package com.zx.mes.upms.hyl.service.impl;

import com.zx.mes.upms.hyl.entity.SysRoleUser;
import com.zx.mes.upms.hyl.mapper.SysRoleUserMapper;
import com.zx.mes.upms.hyl.service.SysRoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色用户关系表 服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUser> implements SysRoleUserService {

}
