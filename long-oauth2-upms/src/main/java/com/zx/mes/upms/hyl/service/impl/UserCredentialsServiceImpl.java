package com.zx.mes.upms.hyl.service.impl;

import com.zx.mes.upms.hyl.entity.UserCredentials;
import com.zx.mes.upms.hyl.mapper.UserCredentialsMapper;
import com.zx.mes.upms.hyl.service.UserCredentialsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户凭证表 服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Service
public class UserCredentialsServiceImpl extends ServiceImpl<UserCredentialsMapper, UserCredentials> implements UserCredentialsService {

}
