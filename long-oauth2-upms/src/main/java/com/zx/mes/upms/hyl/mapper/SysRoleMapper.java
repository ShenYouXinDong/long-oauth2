package com.zx.mes.upms.hyl.mapper;

import com.zx.mes.upms.hyl.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
