package com.zx.mes.upms.hyl.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysServicePermission extends Model<SysServicePermission> {

    private static final long serialVersionUID = 1L;

    /**
     * 服务id
     */
    @TableField("serviceId")
    private Integer serviceId;

    /**
     * 权限id
     */
    @TableField("permissionId")
    private Integer permissionId;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
