package com.zx.mes.upms.hyl.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 微信信息表
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_wechat")
public class Wechat extends Model<Wechat> {

    private static final long serialVersionUID = 1L;

    /**
     * 自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 微信openid
     */
    private String openid;

    /**
     * 微信unionid
     */
    private String unionid;

    /**
     * 绑定用户的id
     */
    @TableField("userId")
    private Integer userId;

    /**
     * 公众号标识
     */
    private String app;

    /**
     * 微信昵称
     */
    private String nickname;

    /**
     * 微信返回的性别
     */
    private String sex;

    /**
     * 微信返回的省
     */
    private String province;

    /**
     * 微信返回的城市
     */
    private String city;

    /**
     * 微信返回的国家
     */
    private String country;

    /**
     * 微信头像
     */
    private String headimgurl;

    /**
     * 创建时间
     */
    @TableField("createTime")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField("updateTime")
    private LocalDateTime updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
