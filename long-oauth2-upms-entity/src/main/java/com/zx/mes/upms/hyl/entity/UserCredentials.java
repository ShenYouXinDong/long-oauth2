package com.zx.mes.upms.hyl.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户凭证表
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserCredentials extends Model<UserCredentials> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名或手机号等
     */
    private String username;

    /**
     * 账号类型（用户名、手机号）
     */
    private String type;

    /**
     * 用户id
     */
    @TableField("userId")
    private Integer userId;


    @Override
    protected Serializable pkVal() {
        return this.username;
    }

}
