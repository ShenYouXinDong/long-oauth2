package com.zx.mes.upms.hyl.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色权限关系表
 * </p>
 *
 * @author 华云龙
 * @since 2018-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysRolePermission extends Model<SysRolePermission> {

    private static final long serialVersionUID = 1L;

    /**
     * 角色id
     */
    @TableId("roleId")
    private Integer roleId;

    /**
     * 权限id
     */
    @TableField("permissionId")
    private Integer permissionId;


    @Override
    protected Serializable pkVal() {
        return this.roleId;
    }

}
