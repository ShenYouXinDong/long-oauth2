package com.zx.mes.demo.hyl.controller;

import com.zx.mes.hyl.response.ObjectRestResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * test controller
 *
 * @author 华云龙
 * @date 2018-12-17
 */
@RestController
@RequestMapping("/test")
@Log4j2
public class TestController {

    @GetMapping("say")
    public ObjectRestResponse<String> say(String str) {
        log.debug("say:" + str);
        return new ObjectRestResponse.Builder<String>().message("成功!").data("say:" + str).build();
    }

}
